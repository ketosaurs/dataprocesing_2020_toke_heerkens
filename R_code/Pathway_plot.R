args = commandArgs(trailingOnly=TRUE)

# A script for the generatin of the pathway relation script.
library("dplyr", quietly=T)
# not in function
'%!in%' <- function(x,y) !("%in%"(x,y))
ficher_exacts <-read.csv(args[1] ,row.names = 1, header = T)


for (i in 1:ncol(ficher_exacts)) {
  for(j in 1:nrow(ficher_exacts)){
    ficher_exacts[j,i] <- -log(ficher_exacts[j,i])
  }
}
rm(i,j)

segnificant_pathways_genepannel <- rownames(ficher_exacts)[ficher_exacts[,ncol(ficher_exacts)] >= 4.54]


# lists
combinations <- combn(ncol(ficher_exacts), 2)
unique_paths <- vector(mode="list",length = ncol(ficher_exacts))
intersection <- vector(mode="list",length = ncol(combinations))
top_ten <- vector(mode="list", length = ncol(ficher_exacts))
uniqe_biggest <- 0
intersection_biggest <- 0


# Open a pdf file to save the pathway plots to
pdf(args[3])


for (i in 1:(ncol(ficher_exacts))) {
  seqnificant_pathways_cluster <- rownames(ficher_exacts)[ficher_exacts[,i] >= 4.54]
  # sort the list and take the 10 elements that and on top
  top_ten[[i]] <- rownames(ficher_exacts[order(-ficher_exacts[i]),][1:10,])
  unique_paths[[i]] <- seqnificant_pathways_cluster[seqnificant_pathways_cluster %!in% segnificant_pathways_genepannel]
  unique_size <-length(unique_paths[[i]])
  if (uniqe_biggest < unique_size){
    uniqe_biggest = unique_size
  }
  for (j in 1:(ncol(ficher_exacts))){
    if (i >= j) {
      next()
    }else{
      # before ploting figure out what axis will be the biggest and use that one makes sure the plots are squar
      if (round(max(ficher_exacts[i])) <= round(max(ficher_exacts[j]))){
        axis_size <- round(max(ficher_exacts[j])+3)
      }else{
        axis_size <- round(max(ficher_exacts[i])+3)
      }
      plot(ficher_exacts[,i], ficher_exacts[,j],
           main = paste("Reactome pathway enrichment analysis of\n", args[2] ,"cluster",i ," vs", args[2] ,"cluster",j),
           sub = "Threshold in red is a Bonferroni corrected value equal to 4.54",
           xlim = c(0,axis_size), xlab = paste("-log(pvalue)", args[2] ,"cluster",i),
           ylim = c(0,axis_size), ylab = paste("-log(pvalue)", args[2] ,"cluster",j)
      )
      abline(h =4.54, col="red")
      abline(v=4.54, col="red")
    }
  }
}
# Close the pdf file
dev.off()


