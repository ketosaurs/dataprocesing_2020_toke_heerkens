# Increase of biological understanding for muli-gene disorders

This is a pipeline that increases biological understanding for multigene disorder based of a clustering of a gene panel

## Installation

Fist create a clean virtual environment and install Snakemake.

```bash
#create a vertual env and activate it
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate

#install snakemake
pip3 install snakemake
```

The second step is to move to the root directory of the project and run the following command.

```bash
Rscript Necessities/Package_instalation.R
```
This will install the necessary R packages for this project this may take a while.

## Usage

In order to run the pipeline simply call the following statement while in the root directory of the project

```bash
snakemake --snakefile Pileline_controler
```

The pipeline uses a config file to run. Two different examples are present inside the root dir. To change which one is used edit the fist line of the Pileline_controler to contain the man of the config file you want to use

## Special remark

The pipeline outputs HTML files of the generated gene networks. These files are stored in the root directory of the project. This happens because of a limitation in the R packages that is used. It has no further influence but can be considered annoying.

## Authors

Toke Heerkens  -  January 2020

